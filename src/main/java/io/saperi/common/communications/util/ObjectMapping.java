package io.saperi.common.communications.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.common.communications.interfaces.IEvent;
import io.saperi.common.communications.data.EventContainer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Base64;

@Slf4j
public class ObjectMapping {

    private static ObjectMapper mapper;


    public static ObjectMapper getMapper() {
        if (mapper == null)
        {
            mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            //Configure the mapper as needed here
        }
        return mapper;
    }


    public static Object unwrapEvent(EventContainer eventContainer) throws ClassNotFoundException, IOException {
        byte[] payload = Base64.getDecoder().decode(eventContainer.getEvent());
        String base = new String(payload);
        StringReader reader = new StringReader(base);
        log.debug("Unwrapping a {}",eventContainer.getType());
        Class type = Class.forName(eventContainer.getType());
        log.debug("Found class {}",type.getCanonicalName());
        log.debug("Payload = {}",base);
        Object out = getMapper().readValue(reader, type);
        return out;

    }

    public static <T> T unwrapEvent(EventContainer eventContainer, T prototype) throws IOException {
        T out = null;
        byte[] payload = Base64.getDecoder().decode(eventContainer.getEvent());
        StringReader reader = new StringReader(new String(payload));
        out = (T) getMapper().readValue(payload, prototype.getClass());
        return out;
    }

    public static EventContainer wrapEvent(IEvent event) throws IOException {
        EventContainer ec = new EventContainer();
        ec.setType(event.getClass().getName());
        StringWriter bldr = new StringWriter();
        getMapper().writeValue(bldr, event);
        ec.setEvent(Base64.getEncoder().encodeToString(bldr.toString().getBytes()));

        return ec;
    }

    public static StringWriter eventToContainerString(IEvent event) throws IOException {
        EventContainer ec = wrapEvent(event);
        StringWriter out = new StringWriter();
        getMapper().writeValue(out,ec);
        return out;
    }

    public static EventContainer getEventContainer(String in)
    {
        EventContainer out;
        StringReader reader = new StringReader(new String(in));
        try {
            out = getMapper().readValue(reader, EventContainer.class);
        } catch (Exception e) {
            //Any exception means we have raw text
            out = new EventContainer();
            out.setType("*raw text*");
            out.setEvent(in);
        }
        return out;
    }

}
