package io.saperi.common.communications.solace;

import com.solacesystems.jcsmp.*;
import io.saperi.common.communications.data.EventContainer;
import io.saperi.common.communications.interfaces.IPubSubListener;
import io.saperi.common.communications.util.ObjectMapping;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class MessageConsumer implements XMLMessageListener {
    private CountDownLatch latch = new CountDownLatch(1);

    private HashMap<String, HashSet<IPubSubListener>> dispatchMap = new HashMap<String, HashSet<IPubSubListener>>();

    public boolean removeTopic(String topic) {
        if (dispatchMap.containsKey(topic)){
            HashSet<IPubSubListener> current = dispatchMap.get(topic);
                dispatchMap.remove(topic);
                return true;
        }
        else
        {
            log.info("Trying to remove an unmapped topic: {}",topic);
        }
        return false;
    }
    public boolean removeListener(String topic, IPubSubListener listener) {
        if (dispatchMap.containsKey(topic)){
            HashSet<IPubSubListener> current = dispatchMap.get(topic);
            if (current.contains(listener))
            {
                dispatchMap.remove(topic);
                return true;
            }
            else
            {
                log.warn("Attempt to remove the wrong listener on {}",topic);
            }
        }
        else
        {
            log.info("Trying to remove an unmapped topic: {}",topic);
        }
        return false;
    }
    public void addListener(String topic, IPubSubListener listener) {
        log.debug("Adding a listener to the message consumer");
        HashSet<IPubSubListener> set;
        if (!dispatchMap.containsKey(topic))
        {
            set = new HashSet<>();
            dispatchMap.put(topic,set);
        }
        else
        {
            set = dispatchMap.get(topic);
        }
        set.add(listener);
    }

    public void onReceive(BytesXMLMessage msg) {
        if (msg instanceof TextMessage) {
            TextMessage txtMsg = (TextMessage) msg;
            String dest = txtMsg.getDestination().getName();
            EventContainer ec = ObjectMapping.getEventContainer(txtMsg.getText());
            if (dispatchMap.containsKey(dest))
            {
                HashSet<IPubSubListener> dispatchList = dispatchMap.get(dest);
                for (IPubSubListener listener: dispatchList)
                {
                    listener.onReceive(dest,ec);
                }
            }
            //TODO:  Deal with pattern matching/wildcards in the future.
        } else {
            log.info("============= Non Text Message received.");
        }
        //latch.countDown(); // unblock main thread
    }

    public void onException(JCSMPException e) {
        //
        log.info("Consumer received exception:", e);
    }

    public CountDownLatch getLatch() {
        return latch;
    }
}
