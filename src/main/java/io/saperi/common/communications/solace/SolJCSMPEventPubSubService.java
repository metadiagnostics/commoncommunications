package io.saperi.common.communications.solace;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solacesystems.jcsmp.*;
import io.saperi.common.communications.interfaces.IEvent;
import io.saperi.common.communications.interfaces.IPubSubListener;
import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.common.communications.listeners.TextListener;
import io.saperi.common.communications.util.ObjectMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


import java.io.StringWriter;
import java.util.HashMap;

/**
 * This a direct implementation of the pub/sub services for Solace JCSMP It is currently in common, but expect to move to it's own library at somepoint.
 */

@Slf4j
@Primary
@Service
public class SolJCSMPEventPubSubService implements IPubSubProvider {


    private SpringJCSMPFactory solaceFactory;
    private JCSMPSession session;


    // TODO: Move to a singlton pattern
    private ObjectMapper objMapper = new ObjectMapper();
    XMLMessageConsumer cons;

    private TextListener defaultListener = new TextListener();
    private MessageConsumer msgConsumer = new MessageConsumer();
    private PublishEventHandler pubEventHandler = new PublishEventHandler();
    private HashMap<String,Topic> topicMap = new HashMap<>();

    public SolJCSMPEventPubSubService(SpringJCSMPFactory solaceFactory) {
        this.solaceFactory = solaceFactory;
    }

    public boolean initialize()  {


        try {
            session = solaceFactory.createSession();
        } catch (InvalidPropertiesException e) {
            log.error("Error creating session", e);
            return false;
        }


        try {
            XMLMessageConsumer cons = session.getMessageConsumer(msgConsumer);
            cons.start();
        } catch (JCSMPException e) {
            log.error("Error creating consumer", e);
            session.closeSession();
            return false;
        }

        return true;
    }

    public boolean cleanup()
    {
        log.info("Pub/Sub Exiting.");
        session.closeSession();
        return true;
    }

    public void createTopic(String topicName)
    {
        if (!topicMap.containsKey(topicName)) {
           Topic topic = JCSMPFactory.onlyInstance().createTopic(topicName);
           topicMap.put(topicName,topic);
        }
    }

    public boolean subscribeToTopic(String topicName)
    {
        return subscribeToTopic(topicName, defaultListener);
    }

    public boolean subscribeToTopic(String topicName, IPubSubListener listener)
    {
        if (topicMap.containsKey(topicName)) {
            Topic topic = topicMap.get(topicName);
            try {
                log.debug("Subscription to Topic "+topicName+" with a listener of type"+listener.getClass().getName());
                session.addSubscription(topic);
                msgConsumer.addListener(topicName, listener);
                //Update msgConsumer
                return true;
            } catch (JCSMPException e) {
                log.warn("Failed to subscribe to "+topicName,e);
                return false;
            }
        }
        log.warn("Topic "+topicName+" unknown, unable to subscribe");
        return false;
    }

    @Override
    public boolean unsubscribeToTopic(String topicName) {
        if (topicMap.containsKey(topicName)) {
            msgConsumer.removeTopic(topicName);
            return true;
        }
        return false;
    }

    @Override
    public boolean unsubscribeToTopic(String topicName, IPubSubListener listener) {
        if (topicMap.containsKey(topicName)) {
            return msgConsumer.removeListener(topicName,listener);
        }
        return false;
    }

    public boolean publishToTopic(String topicName, String msgText)
    {
        if (topicMap.containsKey(topicName)) {
            Topic topic = topicMap.get(topicName);
            TextMessage jcsmpMsg = JCSMPFactory.onlyInstance().createMessage(TextMessage.class);
            jcsmpMsg.setText(msgText);
            jcsmpMsg.setDeliveryMode(DeliveryMode.PERSISTENT);
            XMLMessageProducer prod = null;
            try {
                prod = session.getMessageProducer(pubEventHandler);
                prod.send(jcsmpMsg, topic);
                return true;
            } catch (JCSMPException e) {
                log.error("Error publishing to topic "+topicName,e);
                return false;
            }

        }
        log.warn("Attempt to publish to non-existent topic: "+topicName);
        return false;
    }


    public boolean publishToTopic(String topicName, IEvent event)
    {
        if (topicMap.containsKey(topicName)) {
            Topic topic = topicMap.get(topicName);
            TextMessage jcsmpMsg = JCSMPFactory.onlyInstance().createMessage(TextMessage.class);
            StringWriter out = new StringWriter();
            try {
                jcsmpMsg.setText(ObjectMapping.eventToContainerString(event).toString());
                jcsmpMsg.setDeliveryMode(DeliveryMode.PERSISTENT);
                XMLMessageProducer prod = null;
                try {
                    prod = session.getMessageProducer(pubEventHandler);
                    prod.send(jcsmpMsg, topic);
                    return true;
                } catch (JCSMPException e) {
                    log.error("Error publishing to topic "+topicName,e);
                    return false;
                }
            }
            catch (Throwable e) {
                log.error("Error publishing to topic: "+topicName+", trouble serializing a "+event.getClass().getName(),e);
                return false;
            }



        }
        log.warn("Attempt to publish to non-existent topic: "+topicName);
        return false;
    }


}
