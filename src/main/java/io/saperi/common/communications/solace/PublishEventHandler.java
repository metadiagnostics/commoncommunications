package io.saperi.common.communications.solace;

import com.solacesystems.jcsmp.JCSMPException;
import com.solacesystems.jcsmp.JCSMPStreamingPublishEventHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PublishEventHandler implements JCSMPStreamingPublishEventHandler {
    public void responseReceived(String messageID) {
        log.info("Producer received response for msg: " + messageID);
    }

    public void handleError(String messageID, JCSMPException e, long timestamp) {
        log.trace("Producer received error for msg: %s@%s - %s%n", messageID, timestamp, e);
    }
}
