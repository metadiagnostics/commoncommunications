package io.saperi.common.communications.spring.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.server.WebServer;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.io.StringWriter;
import java.net.InetAddress;
import java.util.Map;

@Slf4j
@Service
public class ServerDataService {
    private ServerProperties serverProperties;

    private String serverRoot;

    private int localPort;

    public ServerDataService(ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    public String getServerRoot() {
        return serverRoot;
    }


    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {

        ApplicationContext applicationContext = event.getApplicationContext();
        if (serverProperties != null) {
            String scheme = serverProperties.getSsl()==null?"http":"https";
            String canonicaHostName = "localhost";
            StringWriter endpoint = new StringWriter();

            InetAddress address = serverProperties.getAddress();
            if (address != null) {
                canonicaHostName = serverProperties.getAddress().getCanonicalHostName();
            }
            endpoint.write(scheme);
            endpoint.write("://");
            endpoint.write(canonicaHostName);
            endpoint.write(":");
            int port = serverProperties.getPort();
            if (port == 0) port = localPort;
            endpoint.write(Integer.toString(port));
            String appName = applicationContext.getApplicationName();
            if (appName != null) {
                endpoint.write(appName);
            }

            log.info("Endpoint: {}", endpoint.toString());
            serverRoot = endpoint.toString();
        }


        String appName = applicationContext.getApplicationName();
        RequestMappingHandlerMapping requestMappingHandlerMapping = applicationContext
                .getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> map = requestMappingHandlerMapping
                .getHandlerMethods();
        map.forEach((key, value) -> log.info("{}/{} {}", appName, key, value));

    }

    @EventListener
    public void onApplicationEvent(final ServletWebServerInitializedEvent event) {
        WebServer webServer = event.getWebServer();
        localPort = webServer.getPort();
    }
}
