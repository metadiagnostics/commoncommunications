package io.saperi.common.communications.interfaces;

import com.solacesystems.jcsmp.*;

import java.io.IOException;
import java.io.StringWriter;

public interface IPubSubProvider {

    public boolean initialize();
    public boolean cleanup();
    public void createTopic(String topicName);
    public boolean subscribeToTopic(String topicName);
    public boolean subscribeToTopic(String topicName, IPubSubListener listener);
    public boolean unsubscribeToTopic(String topicName);
    public boolean unsubscribeToTopic(String topicName, IPubSubListener listener);
    public boolean publishToTopic(String topicName, String msgText);
    public boolean publishToTopic(String topicName, IEvent event);

}
