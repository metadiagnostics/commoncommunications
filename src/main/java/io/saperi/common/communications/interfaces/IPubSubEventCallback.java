package io.saperi.common.communications.interfaces;

public interface IPubSubEventCallback {

    public void handleEvent(String topic, String type, Object obj);
}
