package io.saperi.common.communications.interfaces;

import io.saperi.common.communications.data.EventContainer;

public interface IPubSubListener {
    public void onReceive(String topic, EventContainer eventContainer);
}
