package io.saperi.common.communications.event;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.common.communications.interfaces.IEvent;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
abstract public class EventCommon implements IEvent {


    @JsonIgnore
    private HashMap<String, String> properties = new HashMap<>();

    public EventCommon()
    {

    }

    @JsonIgnore
    public String getEventName()
    {
        return this.getClass().getSimpleName();
    }

    @JsonAnySetter
    public void add(String property, String value){
        properties.put(property, value);
    }

    public Map<String,String> getExtraProperties()
    {
        return properties;
    }

}
