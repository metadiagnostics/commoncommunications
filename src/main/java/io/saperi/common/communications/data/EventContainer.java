package io.saperi.common.communications.data;

import io.saperi.common.communications.interfaces.IEvent;
import lombok.Data;

@Data
public class EventContainer {
    private String type;
    private String event;
}
