package io.saperi.common.communications.listeners;

import io.saperi.common.communications.data.EventContainer;
import io.saperi.common.communications.interfaces.IPubSubListener;
import io.saperi.common.communications.util.ObjectMapping;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class TypedListener<T> implements IPubSubListener {



    @Override
    public void onReceive(String topic, EventContainer eventContainer) {

        try {
            T obj = (T) ObjectMapping.unwrapEvent(eventContainer);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
