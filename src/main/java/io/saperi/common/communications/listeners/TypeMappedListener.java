package io.saperi.common.communications.listeners;

import io.saperi.common.communications.data.EventContainer;
import io.saperi.common.communications.interfaces.IPubSubEventCallback;
import io.saperi.common.communications.interfaces.IPubSubListener;
import io.saperi.common.communications.util.ObjectMapping;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;

@Slf4j
public class TypeMappedListener implements IPubSubListener {
    private HashMap<String, IPubSubEventCallback> callMap = new HashMap<>();

    public void addMapping(String type, IPubSubEventCallback callback)
    {
        callMap.put(type,callback);
    }
    public void addMapping(Class theClass, IPubSubEventCallback callback)
    {
        callMap.put(theClass.getCanonicalName(), callback);
    }
    public void removeMapping(Class theClass)
    {
        callMap.remove(theClass.getCanonicalName());
    }

    @Override
    public void onReceive(String topic, EventContainer eventContainer) {
        String type = eventContainer.getType();
        if (callMap.containsKey(type))
        {
            try {
                Object rawEvent = ObjectMapping.unwrapEvent(eventContainer);
                IPubSubEventCallback cb = callMap.get(type);
                if (cb!=null) {
                    try {
                        cb.handleEvent(topic, type, rawEvent);
                    } catch (Exception e) {
                        log.error("Error handling event {} on topic {}", topic, type);
                        log.error("Error detail: ", e);
                    }
                }
                else
                {
                    log.debug("Type {} suppressed by this handler",type);
                }
            } catch (Exception e) {
                log.error("Problem mapping to event type: "+type,e);
            }
        }
        else {
            log.debug("Type {} skipped as not mapped to this handler",type);
        }
    }

    public class genericUnimplementedHandler implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            log.info("Event {} or Topic {} is still unimplemented!",type,topic);
        }
    }
}
