package io.saperi.common.communications.listeners;

import com.solacesystems.jcsmp.BytesXMLMessage;
import com.solacesystems.jcsmp.JCSMPException;
import com.solacesystems.jcsmp.TextMessage;
import io.saperi.common.communications.data.EventContainer;
import io.saperi.common.communications.interfaces.IPubSubListener;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TextListener implements IPubSubListener  {

    public void onReceive(String topic, EventContainer eventContainer) {
        log.info("============= TextMessage received: " + eventContainer.getEvent() +" from destination "+topic);
    }


}
