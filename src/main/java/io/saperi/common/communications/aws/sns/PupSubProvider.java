package io.saperi.common.communications.aws.sns;

import io.saperi.common.communications.interfaces.IEvent;
import io.saperi.common.communications.interfaces.IPubSubListener;

public class PupSubProvider implements io.saperi.common.communications.interfaces.IPubSubProvider{

    @Override
    public boolean initialize() {
        return false;
    }

    @Override
    public boolean cleanup() {
        return true;
    }

    @Override
    public void createTopic(String topicName) {

    }

    @Override
    public boolean subscribeToTopic(String topicName) {
        return false;
    }

    @Override
    public boolean subscribeToTopic(String topicName, IPubSubListener listener) {
        return false;
    }

    @Override
    public boolean unsubscribeToTopic(String topicName) {
        return false;
    }

    @Override
    public boolean unsubscribeToTopic(String topicName, IPubSubListener listener) {
        return false;
    }

    @Override
    public boolean publishToTopic(String topicName, String msgText) {
        return false;
    }

    @Override
    public boolean publishToTopic(String topicName, IEvent event) {
        return false;
    }
}
